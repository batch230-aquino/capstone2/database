const jwt = require("jsonwebtoken");

const secret = "EcommerceAPI";

//Token Creator
module.exports.createAccessToken = (user) => {
    //payload
    const data = {
        id: user._id,
        email: user._email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {}) //expiresIn: "60s" could be inside those bracket
}


// Token Verifier
// To verify a token from the request (from postman)
module.exports.verify = (req, res, next) =>{

	// The token is retrieved from the request header.
	let token = req.headers.authorization;

	// console.log(token);
	// If token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers.
	if (token !== undefined){
		// res.send({message: "Token received!"})

		// The token sent is a type of "Bearer " which when received contains the "Bearer" as a prefix to the string. To remove the "Bearer " prefix we used the slice method to retrieve only the token.
		token = token.slice(7, token.length);

		console.log(token);

		// Vaidate the "token" using the "verify" method to decrypt the token using the secret code.
		// Syntax: jwt.verfiy(token, secretOrPrivateKey, [options/callBackFunction])

		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return res.send({auth: "Invalid token!"});
			}
			// If JWT is valid
			else{
				// Allows the application to proceed with the next middleware function/callback function in the route.
				// Other way to get the payload
				req.userData = data;
				console.log(req.userData)
				next();
			}
		})
	}
	else{
		res.send({message: "Auth failed. No token provided!"})
	}
}

// To decode the user details from the token
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		// remove first 7 characters ("Bearer ") from the token
		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) => {
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}