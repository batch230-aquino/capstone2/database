const express = require("express");
const Orders = require("../caps_models/orders");
const Users = require("../caps_models/Users");
const Products = require("../caps_models/Products");
const auth = require("../auth");
/*
module.exports.addToCart = async (req, res) => {

    const userData = auth.decode(req.headers.authorization);

   let  newOrder = new Orders({
        totalAmount: req.body.totalAmount,
        userId: req.body.userId,
        productId: req.body.productId,


   })
    console.log(newOrder);
    
    return newOrder.save()
    .then(order =>{
        orders.products.push({
            productId: data.productId,
            productName: data.productName,
            quantity:data.quantity
        })
        console.log(order);
        res.send("order placed")
    })
    .catch(error =>{
        console.log(error);
        res.send("error on the process")
    })

}
*/  
//test 2
/*
module.exports.processOrders = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);

  let productName = await Products.findById(req.body.productId);
  
    let newOrder = new Orders({
      totalAmount: req.body.totalAmount,
      userId: req.body.userId,
      productId: req.body.productId,
      productName: productName
    });
  
    console.log(newOrder);
  
   
    let updateOrder = await Orders.findById(newOrder.userId)
      .then(orders => {
        order.products.push({
          productId: data.productId,
          productName: data.productName,
          quantity: data.quantity
        });
        console.log(order);
        res.send("order placed");
      })
      .catch(error => {
        console.log(error);
        res.send("error on the process");
      });
  };
  
*/

//test code 3
// module.exports.addToCart = (req, res) =>{
//   const addedProduct = Products.findById(req.body.id);

//   Orders.save(addedProduct);
// }

//add to cart

// module.exports.addToCart = async (req, res) => {
//   try {
//     const userData = auth.decode(req.headers.authorization);
//     const userId = userData.id;

  
//     const product = await Products.findById(req.body.productId);

   
//     const order = new Orders({
//         totalAmount: req.body.totalAmount,
//         userId: userId,
//         products: [{
//         productId: product.id,
//         quantity: req.body.quantity
    
         
//         }]
//     });
    
//     await order.save();

//     return res.send({
//       message: 'Order successfully placed',order: order
//     });
//   } 
//     catch (error) {
//     console.log(error);
//     return res.send("Failed to process the order");
//   }
// }



module.exports.addToCart = async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const userId = userData.id;

  
    const product = await Products.findById(req.body.productId);

   
    const order = new Orders({
        totalAmount: product.price,
        userId: userId,
        products: [{
        productId: product._id,
        productName: product.productName
        }]
    });
    
    
    await order.save();

    return res.send({
      message: 'Order successfully placed',order: order
    });
  } 
    catch (error) {
    console.log(error);
    return res.send("Failed to process the order");
  }
}






//Cart of allunpaid orders
module.exports.allUnpainOrders = (req, res) =>{
   
const userData = auth.decode(req.headers.authorization);

let subTotal = []



  return Orders.find({userId:userData.id, isPaid:false})
  .then(result =>{
    result.forEach(perResult =>{
      console.log(perResult.totalAmount)

      subTotal.push(perResult.totalAmount)
      console.log(subTotal)   

      
    })

    let sumOfSubTotal = subTotal.reduce((x, y)=>{
      return x + y;
    });
    console.log(sumOfSubTotal);

    res.send(`${result}\n All Pending Product Total Price:${sumOfSubTotal}`);
    
    
  })
  .catch(error =>{
    console.log(error);
    res.send("Error to retrieve all orders")
  })
};


//all orders retrieval//

module.exports.checkOutOrders = (req, res) =>{
   
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
      return Orders.find({isPaid:true})
      .then(result =>{
        console.log(result);
        res.send(result);
      })
      .catch(error =>{
        console.log(error);
        res.send("Error to retrieve all orders")
      })
    }
    else{
      console.log("Only admin has the access to this function.")
    }
};

//update orders (admin only) Pending

module.exports.updatingCart = (req, res) =>{
  
  const  userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){
    return Orders.findByIdAndUpdate(req.params.orderId, {isPaid:req.body.isPaid})
    .then(result =>{
      console.log(result);
      res.send("updated paid checkout")
    })
    .catch(error =>{
      console.log(error);
      res.send("failed to update")
    })
    }

  
   

  }







//retrieve auth user's order

module.exports.userOrders = (req, res) => {
  
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
      return Orders.findById(req.body.orderId)
      .then(result =>{
        console.log(result);
        res.send(result)
      })
      .catch(error =>{
        console.log(error);
        res.send("Order Id not found.")
      })

    }
    else{
      console.log("Only Admin has the access to this function.")
  
  }
    
}

