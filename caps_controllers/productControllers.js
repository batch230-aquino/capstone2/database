const Products = require("../caps_models/Products");
const auth = require("../auth");


// Only Admin can access
module.exports.addProducts = (req, res) =>
{
    const  userData = auth.decode(req.headers.authorization);


    let newProducts = new Products({
        productName: req.body.productName,
        description: req.body.description,
        price:req.body.price,
        stocks: req.body.stocks
    });

    if(req.userData.isAdmin){
        
        return newProducts.save()
        .then(products =>{
            console.log(products)
            res.send("Added New Product!")
        })
        .catch(error =>{
            console.log(error);
            res.send("Failed to add Product!")
        })
    }
    else{
        return res.status(404).send("Only Admin has access to this function!");
    }
};

//retrieve all items

module.exports.getAll = (req, res)=>{
    return Products.find({isActive:true})
    .then(result => res.send(result));
}

//retrieve single items
module.exports.specificItem = (req, res) =>{
    console.log(req.params.productId);
    return Products.findById(req.params.productId).then(result => res.send(result));
}

//Update product information
module.exports.updateItem = (req, res) =>{
    const  userData = auth.decode(req.headers.authorization);
    if(req.userData.isAdmin){
        let updatedItem = {
            productName: req.body.productName,
            description: req.body.description,
            price: req.body.price,
            slots: req.body.slots
        }

        return Products.findByIdAndUpdate(req.params.productId, updatedItem, {new:true})
        .then(result =>{ 
            console.log(result);
            res.send(result);
        })
        .catch(error =>{
            console.log(error);
            res.send("failed to update the specific item");
        })
    }
};

//Archive product

module.exports.saveProduct = (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        return Products.findByIdAndUpdate(req.params.productId, { isActive: req.body.isActive })
        .then(result => res.send("Product is save to the archive"))
        .catch(error => res.send("failed to save the product to the archive"));
    }
    else{
        return res.status(404).send("You don't have access to this page!");
    }
}


