const User = require("../caps_models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Products = require("../caps_models/Products");
const Orders = require("../caps_models/orders");

// registration
module.exports.userRegistration = (req, res) =>
{
    let newUser = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
    })

    console.log(newUser);

    return newUser.save()

    .then(user => {
        console.log(user);
        res.send("Registration Successful");
    })
    .catch(error => {
        console.log(error);
        res.send("Error! Failed Registration")
    })
}

    

//login

module.exports.userLogin = (req, res) =>{
    return User.findOne({email: req.body.email})
    .then(result =>{
        if(result == null){
            return res.send("Email does not exist.")
        }
        else{
            const isPasswordMatch = bcrypt.compareSync(req.body.password, result.password);

            if(isPasswordMatch){
                
                return res.send({accessToken: auth.createAccessToken(result)});
            }
            else{
                
                return res.send("Password is Incorrect.")
            }
        }
    })
}

//retrieve usersdata
module.exports.getUserDetails = (req, res) =>{

    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){

        return User.findById(req.params.userId).then(result => {
            result.password = '';
            res.send(result);
        }).catch(error =>{
            res.send("User Id not found!")
        })
    }
    
}

//retreive all userdata
module.exports.getAllUser = (req, res)=>{
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        
        return User.find({isAdmin:false})
    .then(result => res.send(result))
    .catch(error =>{
        console.log(error);
        res.send(error);
    })
    }
    
}









   


