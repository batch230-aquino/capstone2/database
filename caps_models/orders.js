const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({
totalAmount:{
    type: Number,
    required:[true, "Total Amount is need to be filled out"]
},
purchaseOn:{
    type: Date,
    default: new Date()
},
userId:{
    type: String,
    required:[true, "User Id is required"]
},
isPaid:{
    type:Boolean,
    default: false
},
products:[
    {
        productId:{
            type: String,
            required:[true, "Product Id is required"]
        },
        productName:{
            type:String
        },
        quantity:{
            type:Number,
            default: 1
            
        }
        
    }
]
})

module.exports = mongoose.model("Orders", orderSchema)
