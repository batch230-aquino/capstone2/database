const express = require("express");
const router = express.Router();
const orderControllers = require("../caps_controllers/orderControllers");
const auth = require("../auth");

router.post("/addtocart", orderControllers.addToCart);

router.get("/checkout", orderControllers.checkOutOrders);

router.get("/userOrder", auth.verify, orderControllers.userOrders);

router.get("/unpaid", auth.verify, orderControllers.allUnpainOrders);

router.patch("/:orderId/updatePaid", auth.verify, orderControllers.updatingCart);

module.exports = router;