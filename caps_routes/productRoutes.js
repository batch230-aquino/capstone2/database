const express = require("express");
const router = express.Router();
const productControllers = require("../caps_controllers/productControllers");
const auth = require("../auth");


router.post("/add", auth.verify, productControllers.addProducts);

router.get("/getAll",  productControllers.getAll);

router.get("/:productId", productControllers.specificItem);

router.put("/:productId/update", auth.verify, productControllers.updateItem);

router.patch("/:productId/archive", auth.verify, productControllers.saveProduct);

module.exports = router;