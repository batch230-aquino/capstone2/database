const express = require("express");
const router = express.Router();
const userControllers = require("../caps_controllers/userControllers");
const auth = require("../auth");


router.post("/Registration", userControllers.userRegistration);

router.post("/login", userControllers.userLogin);

router.get("/:userId/userDetails", auth.verify, userControllers.getUserDetails);

router.get("/Allusers", auth.verify, userControllers.getAllUser);



module.exports = router;